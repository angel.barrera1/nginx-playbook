# nginx-playbook

This project will be used during the concept test developed with the RedHat team. Contains the installation of a nginx server.

## Getting Started
This playbook has been tested on a docker container with an image of ubuntu version 17.

### Prerequisites

To run this playbook it is necessary to have a machine with the ubuntu operating system *(tested on version 17)* and ssh connectivity to it.

```bash
docker pull ubuntu:17.10
```

### Installing

The test performed on a docker container can be replicated as follows:
```
docker run --rm --entrypoint /bin/bash -it -v $(pwd):/tmp/app -w /tmp/app ubuntu:17.10
apt-get update && apt-get install -y ansible curl
ansible-galaxy install -r requirements.yml
ansible-playbook playbook.yml -i "localhost," -c local
```


## Running the tests

```bash
curl http://localhost:80
```

The output should offer a result similar to
```bash
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

### Break down into end to end tests

This test proves that an http server has been built on port 80

## Built With

* [Ansible](https://www.ansible.com/) - Ansible is Simple IT Automation
* [geerlingguy.apache](https://galaxy.ansible.com/geerlingguy/apache/) - Pre built apache role
* [Docker](https://www.docker.com/) - Docker Engine