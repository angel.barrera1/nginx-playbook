FROM ubuntu:17.10

ENV PACKER_VERSION=1.1.2

RUN apt-get update && apt-get install -y wget unzip python python-pip
RUN apt-get install dirmngr --install-recommends -y
RUN pip install awscli==1.14.33
RUN wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip -O packer.zip
RUN unzip packer.zip
RUN mv packer /bin/packer
RUN echo "deb http://ftp.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/backports.list
RUN gpg --keyserver pgp.mit.edu --recv-keys 7638D0442B90D010 8B48AD6246925553
RUN gpg -a --export 7638D0442B90D010 | apt-key add -
RUN gpg -a --export 8B48AD6246925553 | apt-key add -
RUN apt-get update && apt-get upgrade -y 
RUN apt-get install -y build-essential git python python-dev libffi-dev libssl-dev python-pip libyaml-dev libgmp-dev libgmp10 libyaml-0-2 apt-utils rsync
RUN apt-get install -y -t jessie-backports python-pyasn1 python-setuptools
RUN git clone https://github.com/ansible/ansible.git /usr/local/src/ansible

RUN cd /usr/local/src/ansible && git checkout v2.4.2.0-1 && git submodule update --init --recursive && make && make install

USER root

WORKDIR /var/tmp

